﻿USE emcan;

/*
  (18) En qué municipio hay más centros de formación
*/
  SELECT municipio, COUNT(*) numero FROM centros GROUP BY municipio;
  SELECT MAX(numero) FROM (SELECT municipio, COUNT(*) numero FROM centros GROUP BY municipio) c1;

  -- final
  SELECT 
    municipio 
    FROM (SELECT municipio, COUNT(*) numero FROM centros GROUP BY municipio) c1
    JOIN (SELECT MAX(numero) maximo FROM (SELECT municipio, COUNT(*) numero FROM centros GROUP BY municipio) c1) c2
    ON c1.numero=c2.maximo;


  -- solucion entrenador
  SELECT 
    municipio 
  FROM centros
  GROUP BY 1
  HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT municipio, COUNT(*) n FROM centros GROUP BY 1) c1);


/*
  (04) ¿Qué centro impartirá la mayor cantidad de cursos?
*/

  SELECT cod_centro, COUNT(*) n FROM programacion GROUP BY cod_centro;

  SELECT MAX(n) FROM (
  SELECT cod_centro, COUNT(*) n FROM programacion GROUP BY 1) c1;
  
  SELECT centro 
    FROM programacion JOIN
     centros USING (cod_centro)
    GROUP BY 1 
      HAVING COUNT(*)=(SELECT MAX(n) FROM (
        SELECT cod_centro, COUNT(*) n FROM programacion GROUP BY 1) c1)
  ;

  -- solucion entrenador
  
  SELECT centro FROM (SELECT cod_centro FROM programacion GROUP BY 1 
      HAVING COUNT(*)=(SELECT MAX(n) FROM (
        SELECT cod_centro, COUNT(*) n FROM programacion GROUP BY 1) c1)
    ) c2 JOIN centros USING (cod_centro);


/*
  (14) ¿De qué familia profesional han subvencionado más cursos?
*/



/*
  (15) ¿En qué familia profesional está especializado cada centro? Indíquese centro, municipio y familia profesional; y ordénese por municipio y especialidad
*/
/*
  (16) ¿En qué familia profesional está especializado cada municipio? Muestra y ordena por familia y municipio
*/
/*
  (05) ¿Qué centro impartirá la mayor cantidad de cursos en Santander?
*/
/*
  (06) ¿Qué centro impartirá la mayor cantidad de cursos de informática en Cantabria?
*/
/*
  (11) ¿Cuál es el curso de informática que más se impartirá?
*/

  -- c1: cuento por codigo los cursos programados de la familia informatica
  SELECT codigo, COUNT(*) n FROM programacion WHERE SUBSTR(codigo, 1,3)=(SELECT fml FROM familias WHERE familia LIKE '%informatica%') GROUP BY 1;

  -- c2: calculo el maximo 
  SELECT MAX(n) FROM (SELECT codigo, COUNT(*) n FROM programacion WHERE SUBSTR(codigo, 1,3)=(SELECT fml FROM familias WHERE familia LIKE '%informatica%') GROUP BY 1)c1;

  -- calculo el código
  SELECT codigo FROM programacion WHERE SUBSTR(codigo, 1,3)=(SELECT fml FROM familias WHERE familia LIKE '%informatica%') GROUP BY 1 
    HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT codigo, COUNT(*) n FROM programacion WHERE SUBSTR(codigo, 1,3)=(SELECT fml FROM familias WHERE familia LIKE '%informatica%') GROUP BY 1)c1);

  -- busco la especialidad en la tabla cursos
  SELECT especialidad FROM (
    SELECT codigo FROM programacion WHERE SUBSTR(codigo, 1,3)=(SELECT fml FROM familias WHERE familia LIKE '%informatica%') GROUP BY 1 
    HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT codigo, COUNT(*) n FROM programacion WHERE SUBSTR(codigo, 1,3)=(SELECT fml FROM familias WHERE familia LIKE '%informatica%') GROUP BY 1)c1)
    ) c1
    JOIN 
    cursos USING (codigo);

  -- solucion entrenador 

  SELECT especialidad FROM 
    (SELECT codigo FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN programacion ON fml=SUBSTR(codigo, 1,3) 
      GROUP BY 1 HAVING COUNT(*)=
        (SELECT MAX(n) 
          FROM 
    (SELECT codigo, COUNT(*) n FROM 
    (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN programacion ON fml= SUBSTR(codigo, 1,3)GROUP BY 1) c2)) c3 
  JOIN cursos USING (codigo);



-- MySQL:
SELECT especialidad FROM (
    SELECT codigo FROM (
        SELECT fml FROM familias
          WHERE familia LIKE '%informática%'  
      ) c1 JOIN programacion
      ON fml=SUBSTR(codigo,1,3)
      GROUP BY 1 HAVING COUNT(*)=(
        SELECT MAX(n) FROM (
            SELECT codigo,COUNT(*) n FROM (
                SELECT fml FROM familias
                  WHERE familia LIKE '%informática%'  
              ) c1 JOIN programacion
              ON fml=SUBSTR(codigo,1,3)
              GROUP BY 1  
          ) c2  
      )  
  ) c3 JOIN cursos USING(codigo);


/*
  (08) ¿Qué centro impartirá la mayor cantidad de cursos de informática de nivel 3 en Cantabria?
*/
-- c1: codigo IFC y nivel 3
  SELECT fml FROM familias WHERE familia LIKE '%informática%';
  
  SELECT * FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN  cursos 
  ON fml=SUBSTR(codigo,1,3) AND nivel=3;


-- c2: contar por centro
  SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) AND nivel=3 JOIN programacion USING (codigo) GROUP BY 1;

  -- maximo
  SELECT MAX(n) FROM (SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) AND nivel=3 JOIN programacion USING (codigo) GROUP BY 1) c2;


  -- comparo números para sacar el mayor, el cod_centro
  SELECT cod_centro FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) AND nivel=3 JOIN programacion USING (codigo) GROUP BY 1 
  HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) AND nivel=3 JOIN programacion USING (codigo) GROUP BY 1) c2);
  

  -- final: combino con centros para sacar nombre del centro
  SELECT centro FROM 
    (SELECT cod_centro FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos
    ON fml=SUBSTR(codigo,1,3) AND nivel=3 
      JOIN programacion USING (codigo) GROUP BY 1
        HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 
          JOIN cursos ON fml=SUBSTR(codigo,1,3) AND nivel=3 JOIN programacion USING (codigo) GROUP BY 1) c2)) c3 JOIN centros USING (cod_centro);


/*
  (07) ¿Qué centros impartirán la mayor cantidad de cursos de informática en Santander?
*/
  -- c1: codigo IFC
  SELECT fml FROM familias WHERE familia LIKE '%informática%';
  
  SELECT * FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN  cursos 
  ON fml=SUBSTR(codigo,1,3) ;

  -- c2: centros de santander
  SELECT cod_centro FROM centros WHERE municipio='Santander';

  -- c2: contar por centro
  SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) JOIN programacion USING (codigo) JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro) GROUP BY 1;

  -- maximo
  SELECT MAX(n) FROM (SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) JOIN programacion USING (codigo) JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro)) c3;


  -- comparo números para sacar el mayor, el cod_centro
  SELECT cod_centro FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) JOIN programacion USING (codigo) JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro) GROUP BY 1 
  HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos  
  ON fml=SUBSTR(codigo,1,3) JOIN programacion USING (codigo) JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro) GROUP BY 1) c2);


  -- final: combino con centros para sacar nombre del centro
  SELECT centro FROM 
    (SELECT cod_centro FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 JOIN cursos
    ON fml=SUBSTR(codigo,1,3) 
      JOIN programacion USING (codigo) JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro) GROUP BY 1 
        HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 
          JOIN cursos ON fml=SUBSTR(codigo,1,3) JOIN programacion USING (codigo) JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro) GROUP BY 1) c2)) c3 JOIN centros USING (cod_centro);


  -- solucion entrenador
  SELECT centro FROM 
    (SELECT cod_centro FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 
    JOIN programacion ON fml=SUBSTR(codigo,1,3) 
      JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro) GROUP BY 1 
        HAVING COUNT(*)=
          (SELECT MAX(n) FROM (SELECT cod_centro, COUNT(*) n  FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1 
          JOIN programacion ON fml=SUBSTR(codigo,1,3) JOIN (SELECT cod_centro FROM centros WHERE municipio='Santander') c2 USING (cod_centro) GROUP BY 1) c3)) c4 
          JOIN centros USING (cod_centro);