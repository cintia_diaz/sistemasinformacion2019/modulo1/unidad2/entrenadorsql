USE provincias;

-- 2� prueba: 26-08-2019

/*
  (17) �Qu� provincias est�n en autonom�as con nombre compuesto?
*/

  SELECT 
    provincia 
  FROM provincias 
  WHERE autonomia LIKE '% %';


/*
  (18) �Qu� provincias tienen nombre compuesto?
*/
SELECT 
  provincia 
FROM provincias 
WHERE provincia 
LIKE '% %';


/*
  (19) �Qu� provincias tienen nombre simple?
*/
SELECT 
  provincia 
FROM provincias 
WHERE provincia NOT LIKE '% %';

/*
  (51) Muestra las provincias de Galicia, indicando si es Grande, Mediana o Peque�a en funci�n de si su poblaci�n supera el umbral de un mill�n o de medio mill�n de habitantes.
*/

  SELECT provincia, 
    CASE 
      WHEN poblacion>1e6 THEN 'Grande'
      WHEN poblacion>5e5 THEN 'Mediana' 
      ELSE 'Peque�a'
    END
  FROM provincias 
  WHERE autonomia='Galicia';

  /*
    (10) Autonom�as terminadas en 'ana'
*/
SELECT 
  DISTINCT autonomia 
FROM provincias 
WHERE autonomia 
LIKE '%ana';

/*
  (11) �Cu�ntos caracteres tiene cada nombre de comunidad aut�noma? Ordena el resultado por el nombre de la autonom�a de forma descendente
*/
SELECT 
  DISTINCT autonomia, CHAR_LENGTH(autonomia) 
FROM provincias 
ORDER BY autonomia DESC;

/*
  (16) �Qu� autonom�as tienen provincias de m�s de un mill�n de habitantes? Ord�nalas alfab�ticamente
*/

SELECT 
  DISTINCT autonomia 
FROM provincias 
WHERE poblacion>1e6
ORDER BY autonomia;

/*
  (21) Poblaci�n del pa�s
*/
SELECT 
  SUM(poblacion) 
FROM provincias;

/*
  (23) �Cu�ntas provincias hay en la tabla?
*/
SELECT 
  COUNT(*) 
FROM provincias;

SELECT * FROM provincias p;

/*
  (25) �Qu� comunidades aut�nomas contienen el nombre de una de sus provincias?
*/
SELECT autonomia, LOCATE(provincia, autonomia) FROM provincias;

SELECT 
  DISTINCT autonomia 
FROM provincias 
WHERE LOCATE(provincia, autonomia)>0;