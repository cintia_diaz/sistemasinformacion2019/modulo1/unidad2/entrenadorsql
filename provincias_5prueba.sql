﻿USE provincias;

-- 5ª prueba: 27/08/2019

/*
  (53) Obtén el listado de autonomías (una línea por autonomía), junto al listado de sus provincias en una única celda. Recuerda que, tras una coma, debería haber un espacio en blanco.
*/
SELECT 
  autonomia, GROUP_CONCAT(provincia SEPARATOR ', ') 
FROM provincias 
GROUP BY autonomia;

-- Solución entrenador:
SELECT 
  autonomia, GROUP_CONCAT(provincia SEPARATOR ', ') 
FROM provincias 
GROUP BY 1;


/*
  (39) Listado del número de provincias por autonomía ordenadas de más a menos provincias y por autonomía en caso de coincidir
*/

SELECT 
  COUNT(*) n, autonomia 
FROM provincias 
GROUP BY autonomia 
ORDER BY n DESC, autonomia;


/*
  (40) ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?
*/
SELECT 
  autonomia, COUNT(*) 
FROM provincias 
WHERE provincia LIKE '% %' 
GROUP BY autonomia;


/*
  (41) Autonomías uniprovinciales
*/

SELECT 
  autonomia 
FROM provincias 
GROUP BY autonomia 
HAVING COUNT(*)=1;

/*
  (42) ¿Qué autonomía tiene 5 provincias?
*/
SELECT 
  autonomia 
FROM provincias 
GROUP BY autonomia 
HAVING COUNT(*)=5;

/*
  (43) Población de la autonomía más poblada
*/
SELECT autonomia, SUM(poblacion) s FROM provincias GROUP BY autonomia;

SELECT 
  MAX(s) m 
FROM (
  SELECT autonomia, SUM(poblacion) s FROM provincias GROUP BY autonomia
  ) c1;

/*
SELECT autonomia, SUM(poblacion) s 
FROM provincias 
GROUP BY autonomia 
HAVING SUM(poblacion)=(
  SELECT MAX(s) m FROM (SELECT autonomia, SUM(poblacion) s FROM provincias GROUP BY autonomia) c1
   );
*/
-- solucion entrenador
SELECT MAX(pob) FROM (SELECT SUM(poblacion) pob FROM provincias GROUP BY autonomia) c1;

/*
  (44) ¿Qué porcentaje del total nacional representa Cantabria en población y en superficie?
*/

SELECT 
  poblacion/(SELECT SUM(poblacion) FROM provincias)*100, 
  superficie/(SELECT SUM(superficie) FROM provincias)*100 
FROM provincias 
WHERE provincia='Cantabria'; 

-- Solucion entrenador
SELECT 
  (SELECT poblacion FROM provincias WHERE provincia='Cantabria')/(SELECT SUM(poblacion) FROM provincias)*100 pob,
  (SELECT superficie FROM provincias WHERE provincia='Cantabria')/(SELECT SUM(superficie) FROM provincias)*100 sup;


/*
  (49) Obtener la provincia más poblada de cada comunidad autónoma, 
  indicando la población de ésta. 
  Mostrar autonomía, provincia y población por orden de aparición en la tabla.
*/


SELECT autonomia, MAX(poblacion) FROM provincias GROUP BY autonomia;

-- final
SELECT autonomia, provincia, poblacion
  FROM (SELECT autonomia, MAX(poblacion) m FROM provincias GROUP BY autonomia) c1 
  JOIN provincias
  ON c1.max=provincias.poblacion AND c1.autonomia=provincias.autonomia;

-- Solucion Entrenador
SELECT autonomia, provincia, poblacion
  FROM (SELECT autonomia, MAX(poblacion) poblacion FROM provincias GROUP BY 1) c1 
  JOIN provincias
  USING(autonomia, poblacion);



/*
  (47) ¿En qué posición del ranking autonómico por población de mayor a menor está Cantabria?
*/
SELECT 
  autonomia, SUM(poblacion) 
FROM provincias 
GROUP BY autonomia;

SELECT 
  COUNT(autonomia)+1 
FROM (SELECT autonomia, SUM(poblacion) suma FROM provincias GROUP BY autonomia) c1
WHERE suma>(SELECT poblacion FROM provincias WHERE autonomia='Cantabria');


-- Solucion entrenador
SELECT 
  COUNT(*) 
FROM 
  (SELECT autonomia, SUM(poblacion) p FROM provincias GROUP BY autonomia
  HAVING p>=(SELECT SUM(poblacion) FROM provincias WHERE autonomia='Cantabria')
  )c1;




/*
  (45) Automía más extensa
*/
SELECT autonomia, SUM(superficie) sup FROM provincias GROUP BY autonomia;
SELECT MAX(sup) FROM (SELECT autonomia, SUM(superficie) sup FROM provincias GROUP BY autonomia) c1;

-- final
SELECT autonomia
  FROM provincias 
  GROUP BY autonomia 
  HAVING SUM(superficie)=(SELECT MAX(sup) FROM (SELECT autonomia, SUM(superficie) sup FROM provincias GROUP BY autonomia) c1);

-- solucion Entrenador
SELECT 
  autonomia 
  FROM (
  SELECT autonomia, SUM(superficie) sup FROM provincias GROUP BY autonomia
  ) c1 
  WHERE sup=(SELECT MAX(sup) FROM (SELECT autonomia, SUM(superficie) sup FROM provincias GROUP BY autonomia) c1);