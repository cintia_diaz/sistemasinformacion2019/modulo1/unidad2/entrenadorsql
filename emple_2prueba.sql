﻿USE emple_depart;


/*
  (17) Mostrar los datos de los empleados cuyo oficio sea ANALISTA
*/

SELECT 
  * 
FROM emple 
WHERE oficio='ANALISTA';

/*
  (18) Mostrar los datos de los empleados cuyo oficio se ANALISTA y ganen más de 2000
*/
SELECT 
  * 
FROM emple 
WHERE oficio='ANALISTA' AND salario>2000;
/*
  (33) Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre
*/
SELECT 
  apellido 
FROM emple 
ORDER BY oficio, apellido;

/*
  (36) Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. Listar todos los campos de la tabla empleados
*/
SELECT 
  * 
FROM emple 
WHERE apellido NOT LIKE '%z';

/*
  (14) Mostrar el código de los empleados cuyo salario sea mayor que 2000
*/
SELECT 
  emp_no 
FROM emple 
WHERE salario>2000;

/*
  (15) Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000
*/
SELECT 
  emp_no, apellido 
FROM emple 
WHERE salario<2000;

/*
  (16) Mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500
*/
SELECT 
  * 
FROM emple 
WHERE salario BETWEEN 1500 AND 2500;
/*
  (19) Seleccionar el apellido y oficio de los empleados del departamento número 20
*/
SELECT 
  apellido, oficio 
FROM emple 
WHERE dept_no=20;
/*
  (21) Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.
*/
SELECT 
  * 
FROM emple 
WHERE apellido LIKE 'm%' OR apellido LIKE 'n%' 
ORDER BY apellido;
/*
  (22) Seleccionar los datos de los empleados cuyo oficio sea VENDEDOR. Mostrar los datos ordenados por apellido de forma ascendente.
*/
SELECT 
  * 
FROM emple 
WHERE oficio='VENDEDOR'
ORDER BY apellido;