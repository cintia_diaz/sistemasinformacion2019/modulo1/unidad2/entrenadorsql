﻿USE ciclistas;

/*
  (10) ¿Cuál es la distancia total recorrida?
*/
  SELECT 
    SUM(kms) 
  FROM etapa;
/*
  (z10) ¿Cuántos kms se recorren en la vuelta?
*/
   SELECT 
      SUM(kms) 
    FROM etapa;
/*
  (d) Obtener los datos de las etapas que no comienzan en la misma ciudad en que acaba la etapa anterior
*/

  SELECT 
    e1.* 
  FROM etapa e1 
  JOIN etapa e2 
  ON e1.numetapa-1=e2.numetapa 
  AND e2.llegada<>e2.salida;
 

/*
  (b) Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que no se realiza ninguna salida. Resuélvela utilizando NOT IN
*/

  SELECT 
    DISTINCT llegada 
  FROM etapa 
  WHERE llegada NOT IN (SELECT DISTINCT salida FROM etapa);


/*
  (07c) ¿Qué ciclistas han ganado etapas llevando un maillot?
*/
  SELECT DISTINCT dorsal FROM lleva JOIN etapa USING(dorsal, numetapa);

   SELECT 
    nombre
   FROM (SELECT DISTINCT dorsal FROM lleva JOIN etapa USING(dorsal, numetapa)) c1 
   JOIN ciclista 
   USING (dorsal);

-- Solucion entrenador:
  SELECT 
    DISTINCT nombre 
  FROM lleva 
  JOIN etapa 
  USING (numetapa, dorsal) 
  JOIN ciclista 
  USING (dorsal);

/*
  (l) Obtener el nombre de los puertos de montaña que tienen una altura superior a la altura media de todos los puertos
*/
  -- c1
  SELECT AVG(altura) FROM puerto;

  -- final
  SELECT 
    nompuerto 
  FROM puerto 
  WHERE altura>(SELECT AVG(altura) FROM puerto);


/*
  (z5) Número de la etapa más larga
*/
  -- c1
  SELECT MAX(kms) FROM etapa;

  -- final
  SELECT 
    numetapa 
  FROM etapa 
  WHERE kms=(SELECT MAX(kms) FROM etapa);
/*
  (08) ¿Cuántos maillots diferentes ha llevado Miguel Induráin?
*/
  SELECT 
    COUNT(DISTINCT código) 
  FROM ciclista 
  JOIN lleva 
  USING(dorsal) 
  WHERE nombre='Miguel Induráin';

  -- Solucion Entrenador
  SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin';

  SELECT 
    COUNT(DISTINCT código) 
  FROM lleva 
  WHERE dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin');

/*
  (z11) ¿Cuánto dinero se ha repartido en premios?
*/
  SELECT 
    SUM(premio) 
  FROM lleva 
  JOIN maillot 
  USING (código);

 
 
/*
  (a) Obtener los datos de las etapas que pasan por algún puerto de montaña y que tienen salida y llegada en la misma población
*/

SELECT 
  DISTINCT etapa.* 
FROM puerto 
JOIN etapa 
USING (numetapa) 
WHERE salida=llegada;



-- solucion entrenador

SELECT DISTINCT numetapa FROM puerto;

SELECT 
  * 
FROM (SELECT DISTINCT numetapa FROM puerto) c1 
JOIN etapa 
USING (numetapa) 
WHERE salida=llegada;