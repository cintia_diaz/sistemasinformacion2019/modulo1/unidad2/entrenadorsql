﻿USE emple_depart;

-- 4ª prueba: 28-08-2019

/*
  (24) Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. Ordenar el resultado por apellido y oficio de forma ascendente
*/
  SELECT 
    * 
  FROM emple 
  WHERE dept_no=10 AND oficio='ANALISTA' 
  ORDER BY apellido, oficio;
/*
  (25) Realizar un listado de los distintos meses en que los empleados se han dado de alta
*/
  SELECT 
    DISTINCT MONTH(fecha_alt) 
  FROM emple;
/*
  (26) Realizar un listado de los distintos años en los que los empleados se han dado de alta
*/
  SELECT 
    DISTINCT YEAR(fecha_alt) 
  FROM emple;
/*
  (27) Realizar un listado de los distintos días del mes en que los empleados se han dado de alta
*/
  SELECT 
    DISTINCT DAY(fecha_alt) 
  FROM emple;
/*
  (28) Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20
*/
  SELECT 
    apellido 
  FROM emple 
  WHERE salario >2000 OR dept_no=20;
/*
  (35) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados
*/
  SELECT 
    apellido 
  FROM emple 
  WHERE apellido LIKE 'a%' 
  OR apellido LIKE 'm%';
/*
  (34) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados
  Recuerda que LIKE debemos utilizarlo como última opción por su elevado coste computacional. Utiliza SUBSTR()
*/
  SELECT 
    apellido 
  FROM emple 
  WHERE SUBSTR(apellido,1,1) = 'A';
/*
  (37) Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y el oficio tenga una E en cualquier posición.
  Ordenar la salida por oficio y por salario de forma descendente
  Ten en cuenta que %e no localiza los registros que EMPIEZAN por e
*/
  SELECT 
    * 
  FROM emple
  WHERE apellido LIKE 'a%' 
  AND oficio LIKE '%e%' 
  ORDER BY oficio, salario DESC;

  -- solucion entrenador
  SELECT 
    * 
  FROM emple
  WHERE apellido LIKE 'a%' 
  AND LOCATE('e', oficio) -- si devuelve 0, la condicion AND ya no se cumple; si devuelve >0 ya hay una coincidencia
  ORDER BY oficio, salario DESC;

/*
  (6) Indicar el número de empleados que hay
*/
  SELECT 
    COUNT(*) 
  FROM emple;
/*
  (9) Indicar el número de departamentos que hay
*/
SELECT 
  COUNT(*) 
FROM depart;