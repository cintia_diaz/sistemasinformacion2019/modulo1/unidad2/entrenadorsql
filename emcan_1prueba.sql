﻿USE emcan;

/*
  (01) ¿Cuántos centros hay censados en Cantabria?
*/
SELECT 
  COUNT(*) 
FROM centros;

/*
  (02) ¿Cuántos cursos se han programado?
*/
SELECT 
  COUNT(*) 
FROM programacion;

/*
  (03) ¿Cuántos centros han resultado adjudicatarios de estos cursos?
*/
  SELECT 
    COUNT(DISTINCT cod_centro) 
  FROM programacion;

  
/*
  (17) ¿En cuántos municipios se imparten cursos?
*/
  SELECT 
    COUNT(DISTINCT municipio) 
  FROM programacion 
  JOIN centros 
  USING (cod_centro);


/*
    (09) ¿Cuántos cursos de informática se imparten en Cantabria?
*/
 SELECT fml FROM familias WHERE familia LIKE '%informática%';

SELECT 
  codigo 
FROM cursos 
WHERE SUBSTR(codigo, 1, 3)='ifc' ;

SELECT 
  COUNT(codigo) 
FROM programacion 
JOIN (SELECT codigo FROM cursos WHERE SUBSTR(codigo, 1, 3)='ifc') c1 
USING (codigo);


-- solucion Entrenador
SELECT 
  COUNT(codigo) 
FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1
JOIN  programacion 
ON fml=SUBSTR(codigo, 1, 3);


/*
  (10) ¿Cuántos cursos de informática diferentes se imparten en Cantabria?
*/
SELECT 
  COUNT(DISTINCT codigo) 
FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1
JOIN  programacion 
ON fml=SUBSTR(codigo, 1, 3);


/*
  (12) ¿Cuántos cursos de informática de nivel 3 se imparten en Cantabria?
*/

SELECT 
  COUNT(*) 
FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1
JOIN cursos
ON fml=SUBSTR(codigo, 1, 3) AND nivel=3
JOIN programacion  USING (codigo);


/*
  (13) ¿Cuántos cursos de informática de nivel 3 diferentes se imparten en Cantabria?
*/

SELECT 
  COUNT(DISTINCT codigo) 
FROM (SELECT fml FROM familias WHERE familia LIKE '%informática%') c1
JOIN cursos
ON fml=SUBSTR(codigo, 1, 3) AND nivel=3
JOIN programacion  USING (codigo);

SELECT * FROM cursos JOIN programacion USING (codigo);


/*
  (19) Si acabas de cursar mate y lengua N2 o tienes la ESO, ¿dónde tendrías que hacer un curso en Santander para poder cursar en Alpe Aplicaciones Web?
  Indica curso, centro, dirección postal y fecha de inicio, ordenando el listado por ésta última. 
  Recuerda que un certificado de nivel 2 da acceso a un nivel 3 de su misma familia profesional y que, 
  para cursar un nivel 2 necesitas la ESO y un nivel 3 Bachiller, o un ciclo formativo grado medio o superior en formación profesional, respectivamente.
*/

  -- c1
  SELECT SUBSTR(codigo, 1, 3) codigo FROM cursos WHERE especialidad LIKE '%Aplicaciones Web%';

  SELECT codigo, especialidad FROM cursos 
    WHERE SUBSTR(codigo,1,3)= (SELECT SUBSTR(codigo, 1, 3) codigo FROM cursos WHERE especialidad LIKE '%Aplicaciones Web%')
    AND nivel=2;

  
  SELECT cod_centro, especialidad, inicio FROM programacion JOIN (SELECT codigo, especialidad FROM cursos 
    WHERE SUBSTR(codigo,1,3)= (SELECT SUBSTR(codigo, 1, 3) codigo FROM cursos WHERE especialidad LIKE '%Aplicaciones Web%')
    AND nivel=2) c1 USING (codigo);

  SELECT especialidad, centro, direccion, inicio 
    FROM centros 
      JOIN (SELECT cod_centro, especialidad, inicio FROM programacion JOIN (SELECT codigo, especialidad FROM cursos 
            WHERE nivel=2 AND SUBSTR(codigo,1,3)= (SELECT DISTINCT SUBSTR(codigo, 1, 3) FROM cursos WHERE especialidad LIKE '%Aplicaciones%Web%')
             ) c1 USING (codigo)
      ) c1
      USING (cod_centro) 
    WHERE municipio='Santander'
  ORDER BY inicio;

  -- solucion Entrenador
  SELECT especialidad, centro, direccion, inicio 
    FROM programacion 
      JOIN (SELECT codigo, especialidad FROM cursos 
            WHERE nivel=2 AND SUBSTR(codigo,1,3)= (SELECT DISTINCT SUBSTR(codigo, 1, 3)  FROM cursos WHERE especialidad LIKE '%Aplicaciones%Web%')
             ) cursos USING (codigo)
       JOIN (SELECT * FROM centros WHERE municipio='Santander') centros USING (cod_centro)
  ORDER BY inicio;


/*
  (20) Si acabas de cursar mate y lengua N2 o tienes la ESO, ¿dónde tendrías que hacer un curso en Santander para poder cursar en Alpe Formación para el Empleo? 
  Indica curso, centro, dirección postal y fecha de inicio, ordenando el listado por ésta última. 
  Recuerda que un certificado de nivel 2 da acceso a un nivel 3 de su misma familia profesional y que, 
  para cursar un nivel 2 necesitas la ESO y para un nivel 3 Bachiller, o un ciclo formativo grado medio o superior en formación profesional, respectivamente.
*/

  SELECT especialidad, centro, direccion, inicio 
    FROM programacion 
      JOIN (SELECT codigo, especialidad FROM cursos 
            WHERE nivel=2 AND SUBSTR(codigo,1,3)= (SELECT DISTINCT SUBSTR(codigo, 1, 3)  FROM cursos WHERE especialidad LIKE '%Formacion%empleo%')
             ) cursos USING (codigo)
       JOIN (SELECT * FROM centros WHERE municipio='Santander') centros USING (cod_centro)
  ORDER BY inicio, especialidad;