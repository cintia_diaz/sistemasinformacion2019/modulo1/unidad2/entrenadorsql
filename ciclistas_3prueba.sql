﻿USE ciclistas;


 /*
  (20) Etapas con todos sus puertos superando los 1300 metros de altura
 */

  SELECT 
    DISTINCT numetapa 
  FROM puerto
  WHERE altura>1300 
  AND numetapa NOT IN (SELECT numetapa FROM puerto WHERE altura<=1300);

/*
  (07d) ¿Cuántos ciclistas han ganado etapas llevando un maillot?
*/

  SELECT 
    COUNT(DISTINCT dorsal) 
  FROM etapa 
  JOIN lleva 
  USING (numetapa, dorsal);

 -- solucion entrenador
  -- c1: 
  SELECT 
    DISTINCT nombre
  FROM etapa 
  JOIN lleva USING (numetapa, dorsal)  
  JOIN ciclista USING (dorsal);

  -- final
  SELECT 
    COUNT(*)
  FROM (SELECT 
          DISTINCT nombre
        FROM lleva 
        JOIN etapa USING (numetapa, dorsal)  
        JOIN ciclista USING (dorsal)) c1;

/*
  (03) Cuántos puertos ha ganado Induráin llevando algún maillot
*/

  SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin';

  SELECT 
    COUNT(*) 
  FROM (SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin') c1
  JOIN puerto 
  USING (dorsal)  
  JOIN lleva 
  USING (dorsal, numetapa);


/*
  (t) ¿Qué equipos no han ganado etapas con puerto de montaña?
*/
  -- c1: ciclistas que han ganado etapas con puerto
  SELECT DISTINCT etapa.dorsal FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa;

  -- c2: equipos con ciclistas que han ganado etapas con puerto
  SELECT nomequipo FROM ciclista JOIN (SELECT DISTINCT etapa.dorsal FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa) c1 USING (dorsal);

  -- final
  SELECT equipo.nomequipo 
    FROM equipo 
    LEFT JOIN (SELECT nomequipo FROM ciclista JOIN (SELECT DISTINCT etapa.dorsal FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa) c1 USING (dorsal)) c1
    ON c1.nomequipo=equipo.nomequipo WHERE c1.nomequipo IS NULL;


  -- solucion entrenador
   SELECT nomequipo 
    FROM equipo 
    WHERE nomequipo NOT IN (SELECT DISTINCT nomequipo FROM (SELECT numetapa FROM puerto) puerto JOIN etapa USING (numetapa) JOIN ciclista USING (dorsal));
    

/*
  (x) ¿Cuántos puertos ha ganado el líder de la montaña?
*/
/*
  (z7) ¿Cuántos kms hay que recorrer para llegar a la etapa con el puerto más alto?
*/
/*
  (z8) ¿Cuántos kms hay que recorrer para llegar a la última etapa con el puerto más alto?
*/
/*
  (02) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot
*/
/*
  (07) Qué maillots ha llevado Induráin en etapas con puerto cuya etapa haya ganado
*/
  SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin';
  
  SELECT 
    COUNT(*) 
  FROM puerto 
  JOIN etapa USING (numetapa, dorsal) 
  JOIN lleva USING (numetapa, dorsal) 
  WHERE lleva.dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin');

  -- Solución entrenador
  SELECT 
    COUNT(*) 
  FROM (SELECT numetapa, dorsal FROM puerto WHERE dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin')) c2
  JOIN lleva USING (numetapa, dorsal);




/*
 (m) Obtener las poblaciones de salida y de llegada de las etapas donde se encuentran los puertos con mayor pendiente
*/
/*
  (n) Obtener el dorsal y el nombre de los ciclistas que han ganado los puertos de mayor altura
*/
-- c1:  puerto de mayor altura
SELECT MAX(altura) FROM puerto;

-- dorsales ganadores de c1
SELECT dorsal FROM puerto WHERE altura=(SELECT MAX(altura) FROM puerto);

-- final: dorsal y nombres de ciclistas
SELECT 
  dorsal, nombre 
FROM (SELECT dorsal FROM puerto WHERE altura=(SELECT MAX(altura) FROM puerto)) c1 
JOIN ciclista
USING (dorsal);
