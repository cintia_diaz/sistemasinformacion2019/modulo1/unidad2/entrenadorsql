USE provincias;

-- 4� prueba: 26-08-2019

/*
  (29) �Cu�nto mide el nombre de autonom�a m�s corto?
*/
SELECT 
  MIN(CHAR_LENGTH(autonomia)) 
FROM provincias;

/*
  (28) Poblaci�n media de las provincias entre 2 y 3 millones de habitantes sin decimales
*/
SELECT 
  ROUND(AVG(poblacion)) 
FROM provincias 
WHERE poblacion BETWEEN 2e6 AND 3e6;

/*
  (33) Listado de autonom�as cuyas provincias lleven alguna tilde en su nombre
*/
SELECT  DISTINCT autonomia 
FROM provincias 
WHERE LOWER(provincia) LIKE '%�%'COLLATE utf8_bin
  OR LOWER(provincia) LIKE '%�%'COLLATE utf8_bin
  OR LOWER(provincia) LIKE '%�%'COLLATE utf8_bin
  OR LOWER(provincia) LIKE '%�%'COLLATE utf8_bin
  OR LOWER(provincia) LIKE '%�%' COLLATE utf8_bin;


/*
  (31) Provincia m�s poblada
*/
  SELECT MAX(poblacion) FROM provincias;

  SELECT provincia 
  FROM provincias 
  WHERE poblacion=(SELECT MAX(poblacion) FROM provincias);


/*
  (32) Provincia m�s poblada de las inferiores a 1 mill�n de habitantes
*/
SELECT
  MAX(poblacion) 
FROM provincias 
WHERE poblacion<1e6;

SELECT provincia 
FROM provincias 
WHERE poblacion =(SELECT
                     MAX(poblacion) 
                  FROM provincias 
                  WHERE poblacion<1e6);

/*
  (34) Provincia menos poblada de las superiores al mill�n de habitantes
*/
SELECT MIN(poblacion) FROM provincias WHERE poblacion>1e6;

SELECT 
  provincia 
FROM provincias 
WHERE poblacion=(SELECT 
                   MIN(poblacion) 
                 FROM provincias 
                 WHERE poblacion>1e6);

/*
  (35) �En qu� autonom�a est� la provincia m�s extensa?
*/
SELECT MAX(superficie) FROM provincias;

SELECT 
  autonomia 
FROM provincias 
WHERE superficie=(SELECT 
                    MAX(superficie) 
                  FROM provincias);

/*
  (36) �Qu� provincias tienen una poblaci�n por encima de la media nacional?
*/
SELECT AVG(poblacion) FROM provincias;

SELECT 
  provincia 
FROM provincias 
WHERE poblacion>(SELECT AVG(poblacion) FROM provincias);

/*
  (37) Densidad de poblaci�n del pa�s
*/
SELECT 
  SUM(poblacion)/SUM(superficie)
FROM provincias;

-- solucion
  SELECT 
    (SELECT SUM(poblacion) FROM provincias)/(SELECT SUM(superficie) FROM provincias);


/*
  (38) �Cu�ntas provincias tiene cada comunidad aut�noma?

*/

  SELECT 
    autonomia, COUNT(*) 
  FROM provincias GROUP BY autonomia;