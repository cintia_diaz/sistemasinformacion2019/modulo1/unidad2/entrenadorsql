﻿USE emple_depart;

  -- 1ª prueba 27/08/2019
/*
  (1) Mostrar todos los campos y todos los registros de la tabla empleado
*/
SELECT * FROM emple;

/*
  (2) Mostrar todos los campos y todos los registros de la tabla departamento
*/
SELECT * FROM depart;

/*
  (5) Mostrar el número, nombre y localización de cada departamento
*/
SELECT * FROM depart;


/*
  (11) Datos de los empleados ordenados por número de departamento descendentemente
*/
SELECT * 
FROM emple 
ORDER BY dept_no DESC;


/*
  (12) Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente
*/
SELECT 
  * 
FROM emple 
ORDER BY dept_no DESC, oficio;

/*
  (13) Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente
*/
SELECT 
  * 
FROM emple 
ORDER BY dept_no DESC, apellido;


/*
  (3) Mostrar el apellido y oficio de cada empleado
*/
SELECT 
  apellido, oficio 
FROM emple;

/*
  (4) Mostrar localización y número de cada departamento
*/
SELECT 
  loc, dept_no 
FROM depart;

/*
  (7) Datos de los empleados ordenados por apellido de forma ascendente
*/
SELECT 
  * 
FROM emple ORDER BY apellido;

/*
  (8) Datos de los empleados ordenados por apellido de forma descendente
*/

  SELECT 
    * 
  FROM emple 
  ORDER BY apellido DESC;