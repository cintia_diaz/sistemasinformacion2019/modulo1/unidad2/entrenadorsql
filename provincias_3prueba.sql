USE provincias;

-- 3� prueba: 26-08-2019
/*
  (12) �Qu� autonom�as tienen nombre compuesto? Ordena el resultado alfab�ticamente en orden inverso
*/

  SELECT 
    DISTINCT autonomia 
  FROM provincias 
  WHERE autonomia 
  LIKE '% %' 
  ORDER BY autonomia DESC;


  /*
    (13) �Qu� autonom�as tienen nombre simple? Ordena el resultado alfab�ticamente en orden inverso
  */
SELECT 
  DISTINCT autonomia 
FROM provincias 
WHERE autonomia 
NOT LIKE '% %' 
ORDER BY autonomia DESC;

/*
  (14) �Qu� autonom�as tienen provincias con nombre compuesto? Ordenar el resultado alfab�ticamente
*/
SELECT 
  DISTINCT autonomia 
FROM provincias 
WHERE provincia LIKE '% %' 
ORDER BY autonomia;

/*
  (15) Autonom�as que comiencen por 'can' ordenadas alfab�ticamente
*/
SELECT 
  DISTINCT autonomia 
FROM provincias
WHERE autonomia LIKE 'can%' 
ORDER BY autonomia;


/*
  (20) Listado de provincias y autonom�as que contengan la letra �
*/

SELECT  
  provincia
FROM provincias 
WHERE provincia LIKE '%�%' COLLATE utf8_bin
UNION 
SELECT  
  autonomia
FROM provincias 
WHERE autonomia LIKE '%�%' COLLATE utf8_bin;
-- union te quita los repetidos, union all no, es para no usar distinct en una de las dos

/*
  (22) Superficie del pa�s
*/

  SELECT 
    SUM(superficie)
  FROM provincias;

/*
  (24) En un listado alfab�tico, �qu� provincia estar�a la primera?
       Evita utilizar ORDER BY y LIMIT, hay un m�todo m�s eficiente
*/
SELECT 
  MIN(provincia) 
FROM provincias;

/*
  (26) �Qu� provincias tienen un nombre m�s largo que el de su autonom�a?
*/
SELECT 
  provincia
FROM provincias 
WHERE CHAR_LENGTH(provincia)>CHAR_LENGTH(autonomia);

/*
  (27) �Cu�ntas comunidades aut�nomas hay?
*/
SELECT 
  COUNT(DISTINCT autonomia) 
FROM provincias;


/*
  (30) �Cu�nto mide el nombre de provincia m�s largo?
*/
SELECT 
  MAX(CHAR_LENGTH(provincia)) 
FROM provincias;