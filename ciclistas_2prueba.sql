﻿USE ciclistas;

/*
  (e) Obtener el número de las etapas que tienen algún puerto de montaña, indicando cuántos tiene cada una de ellas
*/
SELECT
  numetapa, COUNT(*) 
FROM puerto  
GROUP BY numetapa;

/*
  (g) Obtener el nombre y el equipo de los ciclistas que han llevado algún maillot o que han ganado algún puerto. Muestra la lista ordenada
*/

  -- c1: ciclistas que han llevado algún maillot
  SELECT DISTINCT dorsal, nombre, nomequipo FROM lleva JOIN ciclista USING(dorsal);

  -- c2: ciclistas que han ganado algún puerto
  SELECT DISTINCT dorsal, nombre, nomequipo FROM puerto JOIN ciclista USING(dorsal);

  -- final
  SELECT  nombre, nomequipo FROM lleva JOIN ciclista USING(dorsal)
    UNION
  SELECT nombre, nomequipo FROM puerto JOIN ciclista USING(dorsal);

  -- solucion entrenador
  SELECT 
    nombre, nomequipo 
  FROM (SELECT dorsal FROM lleva UNION SELECT dorsal FROM puerto) c1 
  JOIN ciclista 
  USING (dorsal);


/*
  (07b) Qué códigos de maillots ha llevado Alfonso Gutiérrez en los puertos que haya ganado
*/
-- c1
SELECT dorsal FROM ciclista WHERE nombre='Alfonso Gutiérrez';
-- c2
SELECT dorsal, numetapa FROM puerto WHERE dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Alfonso Gutiérrez');
-- final
SELECT 
   código 
FROM (SELECT dorsal,numetapa FROM puerto WHERE dorsal=(SELECT dorsal FROM ciclista WHERE nombre='Alfonso Gutiérrez')) c1 
JOIN lleva 
ON c1.numetapa=lleva.numetapa AND c1.dorsal=lleva.dorsal;

-- solucion entrenador
SELECT código FROM (
    SELECT dorsal FROM ciclista WHERE nombre='alfonso gutierrez'
  ) c1 JOIN puerto USING(dorsal)
  JOIN lleva  USING(dorsal,numetapa);


/*
  (s) ¿Qué equipos no han ganado ningún puerto de montaña?
*/
SELECT dorsal, nomequipo FROM ciclista JOIN puerto USING (dorsal);

SELECT nomequipo
 FROM (SELECT dorsal, nomequipo FROM ciclista JOIN puerto USING (dorsal)) c1  
RIGHT JOIN equipo USING (nomequipo) WHERE c1.nomequipo IS NULL;


/*
SELECT DISTINCT nomequipo FROM puerto JOIN ciclista USING(dorsal);

SELECT DISTINCT nomequipo FROM ciclista WHERE nomequipo NOT IN (
   SELECT DISTINCT nomequipo FROM puerto JOIN ciclista USING(dorsal)
);

SELECT nomequipo FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal
  GROUP BY nomequipo HAVING COUNT(nompuerto)=0;
*/

-- solucion entrenador
SELECT 
  nomequipo 
FROM equipo 
WHERE nomequipo NOT IN (
   SELECT DISTINCT nomequipo FROM puerto JOIN ciclista USING(dorsal)
    );

/*
  (z3) ¿Cuál es la altura y pendiente media de cada categoría de puerto de montaña?
*/
  SELECT 
    categoria, AVG(altura), AVG(pendiente) 
  FROM puerto 
  GROUP BY 1;

/*
  (z4) Nombre de los ciclistas que han ganado puertos de categoría 2
*/

  -- c1
  SELECT DISTINCT dorsal FROM puerto WHERE categoria='2';

  SELECT 
    nombre 
  FROM ciclista 
  WHERE dorsal=(SELECT DISTINCT dorsal FROM puerto WHERE categoria='2');

-- Solucion entrenador
  SELECT 
    nombre 
  FROM (SELECT DISTINCT dorsal FROM puerto WHERE categoria=2) c1 
  JOIN ciclista 
  USING (dorsal);

/*
  (c) Obtener el nombre y el equipo de los ciclistas que han ganado alguna etapa llevando el maillot amarillo, mostrando también el número de etapa
*/
-- c1: codigo del maillot amarillo
SELECT
  código 
FROM maillot 
WHERE color='amarillo';
  
SELECT 
  dorsal, numetapa
FROM lleva 
JOIN etapa 
USING(numetapa, dorsal) 
WHERE código=(SELECT código FROM maillot WHERE color='amarillo');

SELECT 
  nombre, nomequipo, numetapa
FROM (SELECT 
        dorsal, numetapa 
      FROM lleva 
      JOIN etapa 
      USING(numetapa, dorsal) 
      WHERE código=(SELECT código FROM maillot WHERE color='amarillo')) c1 
JOIN ciclista
USING (dorsal);

-- Solucion entrenador

SELECT 
  nombre, nomequipo, numetapa 
FROM (SELECT código FROM maillot WHERE color='amarillo') c1 
JOIN lleva USING (código) 
JOIN etapa USING (numetapa, dorsal) 
JOIN ciclista USING (dorsal);


/*
  (z12) ¿Qué porcentaje de los kms de la vuelta se recorren en etapas circulares?
*/

  SELECT 
    (SELECT SUM(kms) FROM etapa WHERE salida=llegada)/(SELECT SUM(kms) FROM etapa)
    *100;

  /*
(k) Obtener la edad media de los ciclistas que han ganado alguna etapa
  Sin redondeo. Presta atención a los que hayan ganado más de una etapa.
*/
-- c1
SELECT DISTINCT dorsal FROM etapa;

-- final
SELECT
  AVG(edad) 
FROM (SELECT DISTINCT dorsal FROM etapa) c1 
JOIN ciclista 
USING (dorsal);


/*
  (o) Obtener los datos de las etapas cuyos puertos (todos) superan los 1300 metros de altura
*/

SELECT DISTINCT numetapa FROM puerto WHERE altura<=1300;

SELECT DISTINCT puerto.numetapa FROM puerto LEFT JOIN (SELECT nompuerto, numetapa FROM puerto WHERE altura<=1300) c1 ON puerto.nompuerto=c1.nompuerto AND puerto.numetapa=c1.numetapa
WHERE c1.nompuerto IS NULL;

SELECT DISTINCT puerto.numetapa FROM puerto LEFT JOIN (SELECT DISTINCT nompuerto, numetapa FROM puerto WHERE altura<=1300) c1 ON puerto.nompuerto=c1.nompuerto AND puerto.numetapa=c1.numetapa
WHERE c1.nompuerto IS NULL;
SELECT 
  * 
  FROM etapa 
  JOIN (SELECT DISTINCT puerto.numetapa FROM puerto LEFT JOIN (SELECT DISTINCT nompuerto, numetapa FROM puerto WHERE altura<=1300) c1 ON puerto.nompuerto=c1.nompuerto AND puerto.numetapa=c1.numetapa WHERE c1.numetapa IS NULL) c1 
  USING (numetapa);


-- solucion entrenador

SELECT DISTINCT numetapa FROM puerto WHERE numetapa NOT IN (SELECT DISTINCT numetapa FROM puerto WHERE altura<=1300);

SELECT 
  * 
FROM (SELECT DISTINCT numetapa FROM puerto WHERE numetapa NOT IN (SELECT DISTINCT numetapa FROM puerto WHERE altura<=1300)) c1 
JOIN etapa 
USING (numetapa);
