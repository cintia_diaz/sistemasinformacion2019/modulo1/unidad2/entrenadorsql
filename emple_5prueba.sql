﻿USE practica1;

/*
  (20) Contar el número de empleados cuyo oficio sea VENDEDOR
*/
  SELECT 
    COUNT(*) 
  FROM emple 
  WHERE oficio='VENDEDOR';
/*
  (29) Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece. 
  Ordena el resultado por apellido.
*/
  SELECT 
    apellido, dnombre 
  FROM emple 
  JOIN depart 
  USING(dept_no) 
  ORDER BY apellido;
/*
  (30) Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. 
  Ordenar los resultados por apellido de forma descendente.
*/
  SELECT 
    apellido, oficio, dnombre 
  FROM emple 
  JOIN depart 
  USING(dept_no) 
  ORDER BY apellido DESC;
/*
  (23) Mostrar los apellidos del empleado que más gana
*/
  -- c1
  SELECT MAX(salario) FROM emple;
  
  -- final
  SELECT 
    apellido 
  FROM emple 
  WHERE salario=(SELECT MAX(salario) FROM emple);

/*
  (10) Indicar el número de empleados más el número de departamentos
*/
SELECT COUNT(*) FROM emple;

SELECT COUNT(*) FROM depart;

SELECT
 (SELECT COUNT(*) FROM emple)
  +
 (SELECT COUNT(*) FROM depart);

/*
  (31) Listar el número de departamento de aquellos departamentos que tengan empleados y el número de empleados que tengan.
*/
  SELECT 
    dept_no, COUNT(*) 
  FROM emple
  GROUP BY 1;