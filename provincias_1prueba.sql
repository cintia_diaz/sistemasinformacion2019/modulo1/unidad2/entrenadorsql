USE provincias;

  -- 1� prueba: 26-08-2019
/*
  (01) Listado de provincias
*/

SELECT 
  provincia 
FROM provincias;

/*
  (02) �Cu�nto suman 2 y 3?
*/
SELECT 2+3;


/*
  (04) Densidades de poblaci�n de las provincias
*/
SELECT 
  provincia, poblacion/superficie densidad 
FROM provincias;

/*
  (52) Obt�n el listado del nombre de las provincias que tiene cada autonom�a
*/
SELECT 
  autonomia, provincia 
FROM provincias;

/*
  (03) �Cu�nto vale la ra�z cuadrada de 2?
*/
SELECT SQRT(2);

/*
  (05) �Cu�ntos caracteres tiene cada nombre de provincia?
*/

SELECT 
  provincia, CHAR_LENGTH(provincia) 
FROM provincias;


/*
  (06) Listado de autonom�as
*/
SELECT 
  DISTINCT autonomia 
FROM provincias;

/*
  (07) Provincias con el mismo nombre que su comunidad aut�noma
*/
SELECT 
  provincia 
FROM provincias 
WHERE autonomia=provincia;


/*
  (08) Provincias que contienen el diptongo 'ue'
*/
SELECT 
  provincia 
FROM provincias 
WHERE provincia LIKE '%ue%';

/*
  (09) Provincias que empiezan por A
*/
SELECT 
  provincia 
FROM provincias 
WHERE provincia LIKE 'A%';